Koha Connector module for Drupal 7
==================================

Koha Connector is a module that has to be used with OPAC module
(http://drupal.org/project/opac). It provides a connector for Koha ILS
(http://koha-community.org/).

How to use ?
------------

- Please first read OPAC documentation.
- Enable the module.
- Koha Connector is now available in your OPAC server configuration page.

How to configure ?
------------------

All configuration is done in file koha.conf at module tree root.
The module comes with a sample configuration file, which should provide most of
the defaults for UNIMARC installations.
To see what is possible to do with configuration, please read online
documentation: http://drupal.org/node/1836636
